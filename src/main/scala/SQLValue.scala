package freedb

import java.sql.{ResultSet, Connection, PreparedStatement}
import scalaz._
import scalaz.Id.Id

sealed abstract class SQLValue[A]{
  def map[B](f: A => B): SQLValue[B]
}

object SQLValue {

  /** select
   * [[java.sql.Statement#executeQuery]]
   */
  private[freedb] final case class Select[A](
    sql: String, function: ResultSet => A
  ) extends SQLValue[A] {
    def map[B](f: A => B) = copy(function = function andThen f)
  }

  /** update, insert, delete
   * [[java.sql.Statement#executeUpdate]]
   */
  private[freedb] final case class Update[A](
    sql: String, g: PreparedStatement => Any, function: Int => A
  ) extends SQLValue[A] {
    def map[B](f: A => B) = copy(function = function andThen f)
  }

  implicit val instance: Functor[SQLValue] =
    new Functor[SQLValue] {
      def map[A, B](fa: SQLValue[A])(f: A => B) = fa map f
    }
}

object FreeDB {

  def interpreter(con: Connection): InterpreterId =
    new (SQLValue ~> Id) {
      import SQLValue._
      def apply[A](a: SQLValue[A]) = a match {
        case Select(sql, function) =>
          val st = con.prepareStatement(sql)
          try {
            function(st.executeQuery)
          } finally {
            st.close()
          }
        case Update(sql, g, function) =>
          val st = con.prepareStatement(sql)
          g(st)
          try {
            function(st.executeUpdate)
          } finally {
            st.close()
          }
      }
    }

}
