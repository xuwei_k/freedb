package freedb

import java.sql.ResultSet

final class ResultSetTraversable(private val rs: ResultSet) extends Traversable[ResultSet] {

  override def foreach[U](f: ResultSet => U): Unit = {
    try{
      while (rs.next()) {
        f(rs)
      }
    } finally {
      rs.close()
    }
  }

}
