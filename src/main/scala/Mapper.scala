package freedb

import java.sql.ResultSet

trait Mapper[A] { self =>
  def encode(a: A): Map[String, Any]

  def decode(row: ResultSet): A

  final def removeKeys(keys: String*): Mapper[A] =
    new Mapper[A] {
      def encode(a: A) =
        self.encode(a).filterNot(x => keys.contains(x._1))

      def decode(row: ResultSet) =
        self.decode(row)
    }
}

object Mapper {
  @inline def apply[A](implicit A: Mapper[A]): Mapper[A] = A

  def mapper[A1](decoder: ResultSet => A1, encoder: A1 => Map[String, Any]): Mapper[A1] =
    new Mapper[A1] {
      def decode(row: ResultSet) = decoder(row)
      def encode(a: A1) = encoder(a)
    }

  def mapper2[A1, A2, Z](key1: String, key2: String)(constructor: (A1, A2) => Z, extractor: Z => Option[(A1, A2)])(implicit A1: TypeBinder[A1], A2: TypeBinder[A2], B1: TypeUnbinder[A1], B2: TypeUnbinder[A2]): Mapper[Z] =
    mapper(
      row => constructor(A1(row, key1), A2(row, key2)),
      a => {
        val Some((a1, a2)) = extractor(a)
        Map(key1 -> B1(a1), key2 -> B2(a2))
      }
    )

  def mapper3[A1, A2, A3, Z](key1: String, key2: String, key3: String)(constructor: (A1, A2, A3) => Z, extractor: Z => Option[(A1, A2, A3)])(implicit A1: TypeBinder[A1], A2: TypeBinder[A2], A3: TypeBinder[A3], B1: TypeUnbinder[A1], B2: TypeUnbinder[A2], B3: TypeUnbinder[A3]): Mapper[Z] =
    mapper(
      row => constructor(A1(row, key1), A2(row, key2), A3(row, key3)),
      a => {
        val Some((a1, a2, a3)) = extractor(a)
        Map(key1 -> B1(a1), key2 -> B2(a2), key3 -> B3(a3))
      }
    )

}

