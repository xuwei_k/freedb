package freedb

import java.sql.ResultSet
import scalaz._
import scalaz.Isomorphism.{<~>, IsoFunctorTemplate}

final class SQL[A] private (val run: Free[SQLValue, A]) extends AnyVal {

  def eval[F[_]: Monad](interpreter: Interpreter[F]): F[A] =
    run.foldMap(interpreter)

  def map[B](f: A => B): SQL[B] =
    new SQL(run map f)

  def flatMap[B](f: A => SQL[B]): SQL[B] =
    SQL.instance.bind(this)(f)
}

object SQL {

  val iso: SQL <~> ({type l[a] = Free[SQLValue, a]})#l =
    new IsoFunctorTemplate[SQL, ({type l[a] = Free[SQLValue, a]})#l] {
      def from[A](ga: Free[SQLValue, A]) = new SQL(ga)
      def to[A](fa: SQL[A]) = fa.run
    }

  implicit val instance: Monad[SQL] =
    new IsomorphismMonad[SQL, ({type l[a] = Free[SQLValue, a]})#l] {
      def iso = SQL.iso
      val G = Free.freeMonad[SQLValue]
    }

  def apply[A](value: SQLValue[A]): SQL[A] =
    new SQL(Free.liftF(value))

  def select[A](sql: String)(function: ResultSet => A): SQL[A] =
    SQL(SQLValue.Select(sql, function))

  def selectMany[A](sql: String)(function: ResultSetTraversable => A): SQL[A] =
    select(sql)(result => function(new ResultSetTraversable(result)))

  def selectList[A](sql: String)(function: ResultSet => A): SQL[List[A]] =
    selectMany(sql)(_.map(function).toList)

  def selectListM[A](sql: String)(implicit A: Mapper[A]): SQL[List[A]] =
    selectMany(sql)(_.map(A.decode).toList)

  def update[A](sql: String): SQL[Int] =
    SQL(SQLValue.Update(sql, identity, identity))

  def insert[A](tableName: String, values: A*)(implicit A: Mapper[A]): SQL[List[Int]] = {
    import std.list._
    Traverse[List].traverse(values.map(A.encode).toList){ map =>
      val seq = map.toList
      val keys = seq.iterator.map(_._1).mkString(", ")
      val prepare = List.fill(map.size)("?").mkString(",")
      SQL(SQLValue.Update(
        s"""INSERT INTO $tableName ($keys) VALUES ($prepare)""",
        st => seq.zipWithIndex.foreach{ case ((_, v), i) =>
          st.setObject(i + 1, v)
        },
        identity
      ))
    }
  }

}
