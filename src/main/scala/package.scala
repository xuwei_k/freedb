import scalaz._
import scalaz.Id.Id

package object freedb {
  
  type Interpreter[F[_]] = SQLValue ~> F
  type InterpreterId = Interpreter[Id]

}

