package freedb

import scalaz._, syntax.monad._
import java.sql.DriverManager

object FreeDBSpec extends SpecLite {

  private val testTable = "Persons"

  final case class Person(id: Int, name: String)

  object Person {
    implicit val instance: Mapper[Person] =
      Mapper.mapper2("id", "name")(apply, unapply).removeKeys("id")
  }

  private val sampleList = List("aaa", "bbb", "ccc").map(Person(0, _))

  private val program = for {
    r0   <- SQL.update(s"CREATE TABLE $testTable (id integer primary key auto_increment, name varchar(30) NOT NULL)")
    r1   <- SQL.insert(testTable, sampleList: _*)
    list <- SQL.selectListM[Person](s"select * from $testTable")
  } yield (r0, r1, list)

  "test" in {
    try{
      Class.forName("org.h2.Driver")
      val connection = DriverManager.getConnection("jdbc:h2:mem:test")
      try {
        val result = program.eval(FreeDB.interpreter(connection))
        println(result)
        result._3 must_== List(Person(1, "aaa"), Person(2, "bbb"), Person(3, "ccc"))
      } finally {
        connection.close
      }
    }catch {
      case e: Throwable => e.printStackTrace; throw e
    }
  }

}


