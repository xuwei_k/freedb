scalaVersion := "2.11.1"

name := "freedb"

scalacOptions ++= Seq("-deprecation", "-language:_")

libraryDependencies ++= (
  ("org.scalaz" %% "scalaz-core" % "7.1.0-M7") ::
  ("joda-time" % "joda-time" % "2.3") ::
  ("org.joda" % "joda-convert" % "1.6") ::
  Nil
)


libraryDependencies ++= (
  ("com.h2database" % "h2" % "1.4.178") ::
  ("org.scalacheck" %% "scalacheck" % "1.11.4") ::
  Nil
).map(_ % "test")

